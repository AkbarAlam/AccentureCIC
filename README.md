# Project Accenture CIC

## Application features:

This application contains following features 

* Face Recognition
* Speech Recognition

This application has been written on Java 
### Face Recognition

For face recognition, The [OpenCV](http://opencv.org/) library for Java
 
 
### Speech Recognition

This Speech Recognition has two features 

* Speech to Text 
* Text to Speech

For Speech to Text [CMUsphinx](https://cmusphinx.github.io/) has been used 
and for Text to Speech [FreeTTS](https://freetts.sourceforge.io/) has been used via maven



