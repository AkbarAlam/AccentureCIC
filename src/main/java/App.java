import FaceDetection.ConvertImage;
import FaceDetection.DetectFaceAndEye;
import SpeechRecognition.SpeechRecog;
import org.opencv.core.*;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;


/**
 * The following class will be able to detect the face and eyes
 */

public class App {

    private JFrame frame;
    private JLabel label;

    /**
     * GUI
     */
    private void mainGUI(){
        frame= new JFrame("The Camera");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300,300);
        label=new JLabel();
        frame.add(label);
        frame.setVisible(true);
    }


    /**
     * Run a Loop  for face and Eye detection
     */
    private void runLoop(){


        Mat webCamImg = new Mat();
        VideoCapture capture = new VideoCapture(0);
        ConvertImage convertImage = new ConvertImage();

        Image tmpImg;
        capture.set(Videoio.CAP_PROP_FRAME_WIDTH,300);
        capture.set(Videoio.CAP_PROP_FRAME_HEIGHT,300);



        if (capture.isOpened()){
            while (true){
                capture.read(webCamImg);

                /**
                 * Calling out the DetectFaceAndEye Class
                 */

                DetectFaceAndEye faceAndEye = new DetectFaceAndEye();
                faceAndEye.FaceAndEyeDetect(webCamImg);


                if (!webCamImg.empty()){
                    tmpImg= convertImage.convertMatToImg(webCamImg);
                    ImageIcon imageIcon = new ImageIcon(tmpImg,"Captured");
                    label.setIcon(imageIcon);
                    frame.pack();
                }
                else {
                    System.out.println("There is no Frame");
                    break;
                }
            }
        }
        else {
            System.out.println("Nothing ");
        }
    }

    /**
     * ---------------------------- The main --------------------------
     */

    public static void main(String[] args) throws IOException {

        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        App app = new App();
        SpeechRecog sp=new SpeechRecog();

        // starting the speech recognition thread
        sp.start();

        app.mainGUI();
        app.runLoop();


    }

}
