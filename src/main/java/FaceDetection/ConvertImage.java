package FaceDetection;

import org.opencv.core.Mat;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

/**
 * Converting the matrix to image
 * The method type is @BufferedImage
 *
 */
public class ConvertImage {

    public BufferedImage convertMatToImg (Mat matrix){

        int theType = BufferedImage.TYPE_BYTE_GRAY;

        if (matrix.channels()>1){
            theType = BufferedImage.TYPE_3BYTE_BGR;
        }

        int bufferSize = matrix.channels()*matrix.cols()*matrix.rows();
        byte[] newBuffer = new byte[bufferSize];

        matrix.get(0,0,newBuffer);
        BufferedImage image = new BufferedImage(matrix.cols(),matrix.rows(),theType);
        final byte[] pixels = ((DataBufferByte)image.getRaster().getDataBuffer()).getData();
        System.arraycopy(newBuffer,0,pixels,0,newBuffer.length);

        return image;
    }
}
