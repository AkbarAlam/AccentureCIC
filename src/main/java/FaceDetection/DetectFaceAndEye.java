package FaceDetection;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

/**
 * This Class is for detecting face and eyes
 * This class also performs the logic when there is no face any eye detected
 */
public class DetectFaceAndEye extends Thread{

    @Override
    public void run() {

    }

    public void FaceAndEyeDetect(Mat img){

        //String eyePath = "/Users/Gourab/Downloads/opencv/data/haarcascades/haarcascade_eye.xml";

        CascadeClassifier faceCascade = new CascadeClassifier("/Users/Gourab/Downloads/opencv/data/haarcascades/haarcascade_frontalface_alt.xml");
        CascadeClassifier profileCascade = new CascadeClassifier("/Users/Gourab/Downloads/opencv/data/haarcascades/haarcascade_profileface.xml");
        //CascadeClassifier eyeCascade = new CascadeClassifier(eyePath);

        MatOfRect faceRect = new MatOfRect();
        MatOfRect eyeRect = new MatOfRect();
        MatOfRect faceProfile = new MatOfRect();

        faceCascade.detectMultiScale(img,faceRect);
        //eyeCascade.detectMultiScale(img,eyeRect);
        profileCascade.detectMultiScale(img,faceProfile);


        for (Rect rect : faceRect.toArray()) {
            Imgproc.putText(img, "Human",
                    new Point(rect.x, rect.y - 5), 1, 1,
                    new Scalar(0, 0, 225));

            Imgproc.rectangle(img,
                    new Point(rect.x, rect.y),
                    new Point(rect.x + rect.width, rect.y + rect.height),
                    new Scalar(0, 100, 200), 5);

            System.out.println("Face is there");



        }

        for (Rect rectEye : faceProfile.toArray()){
            Imgproc.rectangle(img,
                    new Point(rectEye.x, rectEye.y),
                    new Point(rectEye.x + rectEye.width, rectEye.y + rectEye.height),
                    new Scalar(0, 100, 100), 2);
        }



        /**
         *  after the iteration when there is no face detected it will trigger the GPS system
         *  which means
         *  @param faceRect.toArray() is an array with the data of detected face pixels
         *                           if that array is empty it will trigger  the gps system .
         *
         *  For this section it will only print a statement.
         *  The statement will be replaced with the GPS triggering method.
         */

        if (faceRect.toArray().length == 0){
            System.out.println("HELP");
            Imgproc.putText(img,"HELP",new Point(200,200),1,10,
                    new Scalar(0,0,153),10);
        }
    }
}
