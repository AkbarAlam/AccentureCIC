package SpeechRecognition;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;

import java.io.IOException;

/**
 * This Class has the Speech recognition feature
 */
public class SpeechRecog extends Thread {

    @Override
    public void run() {
        Configuration config = new Configuration();
        config.setAcousticModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us");
        config.setDictionaryPath("/Users/Gourab/Downloads/TAR9505/9505.dic");
        config.setLanguageModelPath("/Users/Gourab/Downloads/TAR9505/9505.lm");


        /**
         * Text toSpeech
         */


        System.setProperty("freetts.voices", "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");
        //System.setProperty("freetts.voices", "com.sun.speech.freetts.en.us.cmu_time_awb.AlanVoiceDirectory");

        TextToSpeech tts = new TextToSpeech(TextToSpeech.VOICE_KEVIN);



        LiveSpeechRecognizer recognizer = null;
        try {
            recognizer = new LiveSpeechRecognizer(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
        recognizer.startRecognition(true);

        SpeechResult result;

        while ((result = recognizer.getResult()) != null) {
            String command = result.getHypothesis();

            if (command.equalsIgnoreCase("hey computer")){
                System.out.println("HI");
                tts.open();
                tts.speak("Hello");


                /**
                 * With greeting it will just open an app
                 * this is just to show the system can manipulate the system via this method
                 */
                try {
                    Runtime.getRuntime().exec("/Applications/Calculator.app/Contents/MacOS/Calculator");
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            else if (command.equalsIgnoreCase("help me")){
                System.out.println("I will get some Help");
                tts.open();
                tts.speak("ok! I will Stop the car and make a E-Call");

            }
            else if (command.equalsIgnoreCase("I don't feel good")){
                tts.open();
                tts.speak("Okey! I will Stop the car and make a E-Call");
            }
        }
    }
    
}
