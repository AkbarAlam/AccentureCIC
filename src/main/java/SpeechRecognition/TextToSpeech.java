package SpeechRecognition;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

/**
 * This class is for Text To speech conversion
 * This is just to give the user a feedback
 */

public class TextToSpeech {

    public static final String VOICE_KEVIN = "kevin";
    public  static final String VOICE_ALAN= "alan";

    private Voice voice;

    /**
     * This is a Constructor method
     * @param voiceName is the voice name for this
     */
    public TextToSpeech(String voiceName) {

        VoiceManager voiceManager = VoiceManager.getInstance();
        voice = voiceManager.getVoice(voiceName);

        /**
         * This condition will just show if the mentioned voice is There or not
         */
        if (voice == null) {
            System.err.println(
                    "Cannot find a voice named "
                            + voiceName + ".  Please specify a different voice.");
            System.exit(1);
        }
    }

    public void speak(String msg) {
        voice.speak(msg);

    }

    public void open() {
        voice.allocate();
    }

    public void close() {
        voice.deallocate();
    }

}
